from core.cli import laboratory
from core.environment import create_environment
from core.errors import ExperimentAlreadyExistsError


if __name__ == '__main__':
    create_environment('development')
    laboratory.launch()
