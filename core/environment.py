from core.experiments.dotify import Dotify
from core.experiments.squarify import Squarify
from core.experiments.stringify import Stringify
from core.experiments.trianglify import Trianglify
from core.services.image_service import ImageService
from core.services.experiments_service import ExperimentsService


dotify = Dotify(name='dotify', block_size_x=7)
squarify = Squarify(name='squarify', block_size_x=15)
trianglify = Trianglify(name='trianglify', layers=3, block_size_x=5)
trianglify_outline = Trianglify(
    layers=3,
    outline=True,
    block_size_x=8,
    name='trianglify_outline',
)
stringify = Stringify(
    layers=2,
    block_size_x=5,
    block_size_y=10,
    name='stringify',
    text_file='./output/steve-jobs.txt',
)

image_service = ImageService()
experiments_service = ExperimentsService(image_service=image_service)


def create_environment(environment_type='development'):
    experiments_service.register_experiment(dotify)
    experiments_service.register_experiment(squarify)
    experiments_service.register_experiment(stringify)
    experiments_service.register_experiment(trianglify)
    experiments_service.register_experiment(trianglify_outline)
