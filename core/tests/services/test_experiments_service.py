import unittest
from mock import Mock, MagicMock
from core.services.experiments_service import ExperimentsService
from core.errors import (
    ExperimentNotFoundError,
    ExperimentAlreadyExistsError,
)


class TestExperimentsService(unittest.TestCase):
    def setUp(self):
        self.service = ExperimentsService(image_service=Mock())

    def test_register_experiment(self):
        mock = MagicMock()
        mock.name = 'mock'

        self.service.register_experiment(mock)
        self.assertEqual(self.service.experiments[0]['name'], mock.name)

    def test_register_duplicate_experiment(self):
        with self.assertRaises(ExperimentAlreadyExistsError):
            mock = MagicMock()
            mock_2 = MagicMock()
            mock_name = 'mock'
            mock.name = mock_name
            mock_2.name = mock_name

            self.service.register_experiment(mock)
            self.service.register_experiment(mock_2)

    def test_retrieve_non_existing_experiment(self):
        with self.assertRaises(ExperimentNotFoundError):
            self.service.get_experiment('foo')
