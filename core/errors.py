class FileTypeNotSupported(ValueError):
    def __init__(self, file_type):
        super().__init__(f'File type {file_type} not supported.')


class ExperimentNotFoundError(ValueError):
    def __init__(self, experiment_name):
        super().__init__(f'Experiment {experiment_name} not found.')


class ExperimentAlreadyExistsError(ValueError):
    def __init__(self, experiment_name):
        super().__init__(f'Experiment {experiment_name} already exists.')
