from random import randint
from core.experiments.base import Base
from core.experiments.generators.points.square import create_square
from core.experiments.utils.color_utils import (
    calculate_mean_color,
    adjust_color_values
)

COLOR_LIGHT_ADJUSTMENT_FACTOR = -0.1


class Squarify(Base):
    def render(self, tensor, draw_layers, co_ordinates, iteration):
        fill = calculate_mean_color(
            tensor=tensor,
            co_ordinates=co_ordinates,
            block_size=self.block_size_x,
        )
        outline = adjust_color_values(
            rgba_tuple=fill,
            color_adjustment_factor=COLOR_LIGHT_ADJUSTMENT_FACTOR
        )

        x_coordinate, y_coordinate = co_ordinates
        square_points = create_square(
            x_coordinate=x_coordinate,
            y_coordinate=y_coordinate,
            block_size=self.block_size_x,
        )

        draw_layers[0].rectangle(
            fill=fill,
            xy=square_points,
            outline=outline if randint(0, 10) >= 8 else fill
        )
