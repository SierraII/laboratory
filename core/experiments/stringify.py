from PIL import ImageFont
from core.experiments.base import Base
from core.experiments.utils.color_utils import (
    adjust_color_values,
    calculate_mean_color,
)

FONT_SIZE = 10
FONT_NAME = 'Avenir.ttc'
BACKRGOUND_COLOR = (29, 25, 23)
COLOR_LIGHT_ADJUSTMENT_FACTOR = 0.0


class Stringify(Base):
    def __init__(self, name, layers, text_file, block_size_x,
                 block_size_y=None):
        with open(text_file, 'r') as file:
            self.text = (
                file.read()
                .lower()
                .replace(' ', '')
                .replace('\n', '')
            )

        super().__init__(
            name=name,
            layers=layers,
            block_size_x=block_size_x,
            block_size_y=block_size_y,
        )

    def pre_render(self, draw_layers, width, height):
        draw_layers[0].rectangle(
            fill=BACKRGOUND_COLOR,
            xy=[(0, 0), width, height],
        )

    def render(self, tensor, draw_layers, co_ordinates, iteration):
        fill = calculate_mean_color(
            tensor=tensor,
            co_ordinates=co_ordinates,
            block_size=self.block_size_y,
        )

        fill = adjust_color_values(
            rgba_tuple=fill,
            color_adjustment_factor=COLOR_LIGHT_ADJUSTMENT_FACTOR
        )

        font = ImageFont.truetype(FONT_NAME, FONT_SIZE)
        draw_layers[0].text(
            co_ordinates,
            self.text[iteration],
            font=font,
            fill=fill
        )
