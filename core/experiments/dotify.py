from random import randint
from core.experiments.base import Base
from core.experiments.generators.points.ellipse import create_ellipse
from core.experiments.utils.color_utils import (
    adjust_color_values,
    calculate_mean_color,
)

RADIUS_FACTOR = 2
COLOR_LIGHT_ADJUSTMENT_FACTOR = -0.2


class Dotify(Base):
    def render(self, tensor, draw_layers, co_ordinates, iteration):
        fill = calculate_mean_color(
            tensor=tensor,
            co_ordinates=co_ordinates,
            block_size=self.block_size_x,
        )
        outline = adjust_color_values(
            rgba_tuple=fill,
            color_adjustment_factor=COLOR_LIGHT_ADJUSTMENT_FACTOR
        )

        x_coordinate, y_coordinate = co_ordinates
        radius = self.block_size_x/RADIUS_FACTOR
        circle_points = create_ellipse(x_coordinate, y_coordinate, radius)

        draw_layers[0].ellipse(
            fill=fill,
            xy=circle_points,
            outline=outline if randint(0, 10) >= 7 else fill
        )
