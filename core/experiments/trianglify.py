from core.experiments.base import Base
from core.experiments.generators.points.square import create_square
from core.experiments.generators.points.triangle import (
    create_left_triangle,
    create_right_triangle,
)
from core.experiments.utils.color_utils import (
    adjust_color_values,
    calculate_mean_color,
    inject_transparence,
)

TRANSPARENCE = 40
OFFSET_FACTOR = 20
COLOR_LIGHT_ADJUSTMENT_FACTOR = -0.2


class Trianglify(Base):
    def __init__(self, name, layers, block_size_x, block_size_y=None,
                 outline=False):
        self.outline = outline
        super().__init__(
            name=name,
            layers=layers,
            block_size_x=block_size_x,
            block_size_y=block_size_x,
        )

    def render(self, tensor, draw_layers, co_ordinates, iteration):
        rgba_mean = calculate_mean_color(
            tensor=tensor,
            co_ordinates=co_ordinates,
            block_size=self.block_size_x,
        )
        darkened_rgba_mean = adjust_color_values(
            rgba_tuple=rgba_mean,
            color_adjustment_factor=COLOR_LIGHT_ADJUSTMENT_FACTOR
        )

        x_coordinate, y_coordinate = co_ordinates
        rectangle_points = create_square(
            x_coordinate=x_coordinate,
            y_coordinate=y_coordinate,
            block_size=self.block_size_x,
        )

        right_triangle_color = inject_transparence(
            rgba=rgba_mean,
            transparence=10,
        )
        right_triangle = create_left_triangle(
            x_coordinate=x_coordinate,
            y_coordinate=y_coordinate,
            block_size=self.block_size_x,
            offset_factor=OFFSET_FACTOR,
        )

        left_triangle = create_right_triangle(
            x_coordinate=x_coordinate,
            y_coordinate=y_coordinate,
            block_size=self.block_size_x,
            offset_factor=OFFSET_FACTOR,
        )

        if self.outline is False:
            draw_layers[0].rectangle(rectangle_points, rgba_mean)
            draw_layers[1].polygon(right_triangle, right_triangle_color)
            draw_layers[2].polygon(left_triangle, rgba_mean)
        else:
            draw_layers[1].polygon(right_triangle, outline=darkened_rgba_mean)
            draw_layers[2].polygon(left_triangle, outline=rgba_mean)
