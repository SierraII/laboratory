from random import randint


def offset_coordinate(xy_tuple, offset_factor):
    return tuple(randint(i, (i + offset_factor)) for i in xy_tuple)
