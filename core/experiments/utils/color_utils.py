import numpy as np


def adjust_color_value(value, factor=None):
    if factor:
        value = int(value + (value * factor))
    return int(max(0, min(value, 255)))


def rgb_to_grey_scale(rgba):
    return np.dot(np.asarray(rgba), [0.299, 0.587, 0.114, 1])


def adjust_color_values(rgba_tuple, color_adjustment_factor=None,
                        transparence=None):
    adjusted_rgba = tuple(
        adjust_color_value(
            value=value,
            factor=color_adjustment_factor
        )
        for value in rgba_tuple
    )

    if transparence is not None:
        return inject_transparence(adjusted_rgba, transparence)

    return adjusted_rgba


def calculate_mean_color(co_ordinates, block_size, tensor,
                         color_adjustment_factor=None, transparence=None):
    y_degree = (co_ordinates[1] + block_size)
    x_degree = (co_ordinates[0] + block_size)
    segregated_rectangle = (
        tensor[co_ordinates[1]:y_degree, co_ordinates[0]:x_degree]
    )

    mean_color_tuple = segregated_rectangle.mean(axis=(0, 1))

    factored_color_tuple = adjust_color_values(
        rgba_tuple=mean_color_tuple,
        color_adjustment_factor=color_adjustment_factor
    )

    if transparence is None:
        transparence = 300

    return inject_transparence(factored_color_tuple, transparence)


def inject_transparence(rgba, transparence):
    return rgba[0], rgba[1], rgba[2], transparence
