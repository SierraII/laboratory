from abc import ABCMeta, abstractmethod


class Base(object, metaclass=ABCMeta):
    def __init__(self, name, block_size_x, layers=1, block_size_y=None):
        self.name = name
        self.layers = layers
        self.block_size_x = block_size_x
        self.block_size_y = (
            block_size_y if block_size_y is not None else block_size_x
        )

    def pre_render(self, draw_layers, width, height):
        pass

    def post_render(self, draw_layers, width, height):
        pass

    @abstractmethod
    def render(self):
        pass

    def start(self, tensor, draw_layers):
        iteration = 0
        width = tensor.shape[1]
        height = tensor.shape[0]
        self.pre_render(draw_layers=draw_layers, width=width, height=height)
        for y_coordinate in range(0, height, self.block_size_y):
            for x_coordinate in range(0, width, self.block_size_x):
                self.render(
                    tensor=tensor,
                    iteration=iteration,
                    draw_layers=draw_layers,
                    co_ordinates=(x_coordinate, y_coordinate),
                )
                iteration += 1
        self.post_render(draw_layers=draw_layers, width=width, height=height)
