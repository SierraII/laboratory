def create_square(x_coordinate, y_coordinate, block_size):
    return [
        x_coordinate,
        y_coordinate,
        x_coordinate + block_size,
        y_coordinate + block_size,
    ]
