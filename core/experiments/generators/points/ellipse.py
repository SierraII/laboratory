def create_ellipse(x_coordinate, y_coordinate, radius):
    return (
        x_coordinate-radius,
        y_coordinate-radius,
        x_coordinate+radius,
        y_coordinate+radius,
    )
