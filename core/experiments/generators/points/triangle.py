from core.experiments.utils.shape_utils import offset_coordinate


def create_right_triangle(x_coordinate, y_coordinate, block_size,
                          offset_factor):
    return [
        offset_coordinate(
            offset_factor=offset_factor,
            xy_tuple=(x_coordinate, y_coordinate + block_size),
        ),
        offset_coordinate(
            offset_factor=offset_factor,
            xy_tuple=(x_coordinate + block_size, y_coordinate + block_size)
        ),
        offset_coordinate(
            offset_factor=offset_factor,
            xy_tuple=(x_coordinate + block_size, y_coordinate)
        )
    ]


def create_left_triangle(x_coordinate, y_coordinate, block_size,
                         offset_factor):
    return [
        offset_coordinate(
            offset_factor=offset_factor,
            xy_tuple=(x_coordinate, y_coordinate)
        ),
        offset_coordinate(
            offset_factor=offset_factor,
            xy_tuple=(x_coordinate + block_size, y_coordinate)
        ),
        offset_coordinate(
            offset_factor=offset_factor,
            xy_tuple=(x_coordinate, y_coordinate + block_size)
        )
    ]
