import numpy as np
from PIL import Image, ImageDraw

IMAGE_MODE = 'RGBA'


class ImageService(object):
    def create_image(self, width, height):
        return Image.new(IMAGE_MODE, (width, height))

    def image_from_tensor(self, tensor):
        return Image.fromarray(tensor, 'RGB')

    def create_image_canvas(self, image_tensor):
        size = image_tensor.shape[1], image_tensor.shape[0]
        image = self.create_image(width=size[0], height=size[1])
        return image, ImageDraw.Draw(image)

    def create_image_tensor(self, file_location):
        image = Image.open(file_location)
        return np.array(image, dtype=np.uint8)

    def flatten_image_layers(self, image_layers):
        image_size = (image_layers[0].width, image_layers[0].height)
        result_image = Image.new(IMAGE_MODE, image_size)
        for image in image_layers:
            result_image = Image.alpha_composite(result_image, image)
        return result_image
