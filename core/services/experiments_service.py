import numpy as np
from core.errors import ExperimentNotFoundError, ExperimentAlreadyExistsError

EXCLUDED_EXPERIMENTS = ['normal', 'skip']


class ExperimentsService(object):
    def __init__(self, image_service):
        self.experiments = []
        self.image_service = image_service

    def register_experiment(self, ctx):
        try:
            self.get_experiment(ctx.name)
            raise ExperimentAlreadyExistsError(ctx.name)
        except ExperimentNotFoundError:
            self._register_experiment(ctx)

    def _register_experiment(self, ctx):
        self.experiments.append({
            'ctx': ctx,
            'name': ctx.name,
            'layers': ctx.layers
        })

    def run(self, file_location, experiments):
        image_tensor = self.image_service.create_image_tensor(
            file_location=file_location
        )
        tensors = np.vsplit(image_tensor, len(experiments))
        results = []

        for index, experiment_name in enumerate(experiments):
            tensor = tensors[index]

            if experiment_name in EXCLUDED_EXPERIMENTS:
                result = self.image_service.image_from_tensor(tensor)
                results.append(result)
            else:
                experiment = self.get_experiment(name=experiment_name)
                draw_layers, image_layers = self.setup_experiment_canvas(
                    tensor=tensor,
                    experiment=experiment,
                    file_location=file_location,
                )

                experiment['ctx'].start(
                    tensor=tensor,
                    draw_layers=draw_layers,
                )

                flattened_image = self.image_service.flatten_image_layers(
                    image_layers=image_layers,
                )
                results.append(flattened_image)
                del draw_layers, image_layers

        # TODO - move rest of function into a service and flesh out.
        final_render, draw = self.image_service.create_image_canvas(
            image_tensor=image_tensor,
        )

        offset = 0
        for result in results:
            final_render.paste(result, (0, offset))
            offset += result.size[1]

        final_render.save('./output/final.png')

    def get_experiment(self, name):
        for experiment in self.experiments:
            if experiment['name'] == name:
                return experiment
        raise ExperimentNotFoundError(name)

    def setup_experiment_canvas(self, experiment, tensor, file_location):
        draw_layers = []
        image_layers = []

        for _ in range(experiment['layers']):
            image, draw = self.image_service.create_image_canvas(
                image_tensor=tensor,
            )
            draw_layers.append(draw)
            image_layers.append(image)

        return draw_layers, image_layers
