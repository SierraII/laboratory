import click
from core.environment import experiments_service


@click.command()
@click.option('--file_location', required=True, help='Image location.')
@click.option('--experiments', default='trianglify', help='Experiment to run.')
def launch(file_location, experiments):
    experiments = (
        ['trianglify_outline', 'trianglify', 'normal', 'squarify', 'dotify']
    )
    experiments_service.run(
        experiments=experiments,
        file_location=file_location,
    )
